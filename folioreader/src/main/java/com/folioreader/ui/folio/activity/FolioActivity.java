/*
 * Copyright (C) 2016 Pedro Paulo de Amorim
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.folioreader.ui.folio.activity;

import android.Manifest;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.DragEvent;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.folioreader.Config;
import com.folioreader.Constants;
import com.folioreader.FolioReader;
import com.folioreader.R;
import com.folioreader.model.HighlightImpl;
import com.folioreader.model.ReadPosition;
import com.folioreader.model.event.MediaOverlayPlayPauseEvent;
import com.folioreader.ui.folio.adapter.FolioPageFragmentAdapter;
import com.folioreader.ui.folio.fragment.FolioPageFragment;
import com.folioreader.ui.folio.presenter.MainMvpView;
import com.folioreader.ui.folio.presenter.MainPresenter;
import com.folioreader.util.AppUtil;
import com.folioreader.util.FileUtil;
import com.folioreader.view.ConfigBottomSheetDialogFragment;
import com.folioreader.view.DirectionalViewpager;
import com.folioreader.view.FolioToolbar;
import com.folioreader.view.FolioToolbarCallback;
import com.folioreader.view.FolioWebView;
import com.folioreader.view.MediaControllerCallback;
import com.folioreader.view.MediaControllerView;

import org.greenrobot.eventbus.EventBus;
import org.readium.r2_streamer.model.container.Container;
import org.readium.r2_streamer.model.container.EpubContainer;
import org.readium.r2_streamer.model.publication.EpubPublication;
import org.readium.r2_streamer.model.publication.link.Link;
import org.readium.r2_streamer.server.EpubServer;
import org.readium.r2_streamer.server.EpubServerSingleton;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.folioreader.Constants.CHAPTER_SELECTED;
import static com.folioreader.Constants.HIGHLIGHT_SELECTED;
import static com.folioreader.Constants.SELECTED_CHAPTER_POSITION;
import static com.folioreader.Constants.TYPE;

//FZSM:Added admob
import com.folioreader.view.WebViewPager;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
//FZSM: added interstitial
import com.google.android.gms.ads.InterstitialAd;
//FZSM: added Applovin as second network, NO mediation for fast switching
import com.applovin.adview.AppLovinInterstitialAd;
import com.applovin.adview.AppLovinInterstitialAdDialog;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.applovin.sdk.AppLovinSdk;


//        extends AppCompatActivity

public class FolioActivity
        extends AppCompatActivity
        implements DialogFrag.OnDialogDismissListener,
        FolioActivityCallback,
        FolioWebView.ToolBarListener,
        MainMvpView,
        MediaControllerCallback,
        FolioToolbarCallback
{

    private static final String LOG_TAG = "FolioActivity";
    public static final int INTERSTITIAL_EVERY = 6; //WARNING: this value needs to be >=3
    private static int adCounter = 0;
    public static final String INTENT_EPUB_SOURCE_PATH = "com.folioreader.epub_asset_path";
    public static final String INTENT_EPUB_SOURCE_TYPE = "epub_source_type";
    public static final String INTENT_HIGHLIGHTS_LIST = "highlight_list";
    public static final String EXTRA_READ_POSITION = "com.folioreader.extra.READ_POSITION";
    private static final String BUNDLE_READ_POSITION_CONFIG_CHANGE = "BUNDLE_READ_POSITION_CONFIG_CHANGE";
    private static final String BUNDLE_TOOLBAR_IS_VISIBLE = "BUNDLE_TOOLBAR_IS_VISIBLE";
    private int lastPosition = -1;



    public enum EpubSourceType {
        RAW,
        ASSETS,
        SD_CARD
    }

    public static final int ACTION_CONTENT_HIGHLIGHT = 77;
    private String bookFileName;
    private static final String HIGHLIGHT_ITEM = "highlight_item";

    private DirectionalViewpager mFolioPageViewPager;
    private FolioToolbar toolbar;

    private int mChapterPosition;
    private FolioPageFragmentAdapter mFolioPageFragmentAdapter;
    private ReadPosition entryReadPosition;
    private ReadPosition lastReadPosition;
    private Bundle outState;
    private Bundle savedInstanceState;

    private List<Link> mSpineReferenceList = new ArrayList<>();
    private EpubServer mEpubServer;

    private String mBookId;
    private String mEpubFilePath;
    private EpubSourceType mEpubSourceType;
    int mEpubRawId = 0;
    private MediaControllerView mediaControllerView;
    private Config.Direction direction = Config.Direction.HORIZONTAL;

    private AdView mAdView;
    private InterstitialAd mAdmobInterstitial;
    private TextView adWarning;
    private String adnetwork;

    boolean ignoreLayoutChange=false;

    private AppLovinInterstitialAdDialog mApplovinInterstitial;
    @Override
    public void onDialogDismissListener(int position) {
//nothing to do here
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adCounter = 0;
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON); //Fix for screen get turned off while reading

        setConfig(savedInstanceState);
        //20181412: New folio activity , avoidng overlap with ads
        setContentView(R.layout.folio_activity_v2);
        this.savedInstanceState = savedInstanceState;

        adnetwork = getString(R.string.ad_network).toLowerCase();
        initializeAds(adnetwork);


        mBookId = getIntent().getStringExtra(FolioReader.INTENT_BOOK_ID);
        mEpubSourceType = (EpubSourceType)
                getIntent().getExtras().getSerializable(FolioActivity.INTENT_EPUB_SOURCE_TYPE);
        if (mEpubSourceType.equals(EpubSourceType.RAW)) {
            mEpubRawId = getIntent().getExtras().getInt(FolioActivity.INTENT_EPUB_SOURCE_PATH);
        } else {
            mEpubFilePath = getIntent().getExtras()
                    .getString(FolioActivity.INTENT_EPUB_SOURCE_PATH);
        }
        mediaControllerView = findViewById(R.id.media_controller_view);
        mediaControllerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                ignoreLayoutChange=true;
            }
        });
        mediaControllerView.setListeners(this);

        if (ContextCompat.checkSelfPermission(FolioActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(FolioActivity.this, Constants.getWriteExternalStoragePerms(), Constants.WRITE_EXTERNAL_STORAGE_REQUEST);
        } else {
            setupBook();
        }

        initToolbar(savedInstanceState);

        adWarning = findViewById(R.id.adWarning);
        adWarning.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                ignoreLayoutChange=true;
            }
        });
        showMsg(); //Show Disclaimer if this is the first time

    }

    /**
     * 20190927: Introducing ad network switching
     * I think admob network was more than enough limiting my income.
     * I have to fight and preserve my work and money.
     */

    private void initializeAds(String adn) {
        //There are at least three options: admob, applovin, empty and none.
        //if empty defaults to admob
        //if none (i don't know why I would use this exactly) the ads are disabled.
        switch (adn) {
            case "":
                initializeAdmobAds();
                break;
            case "admob":
                initializeAdmobAds();
                break;
            case "applovin":
                AppLovinSdk.initializeSdk(getApplicationContext());
                initializeAppLovinAds();
                break;
            case "none": //no initialization.
                break;

            default:
                break;
        }

    }

    //Display msg for one time only.
    //Discuss if we need to display every n-restarts.
    private void showMsg() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        //SharedPreferences.Editor editor = pref.edit();
        if (!pref.getBoolean("showMsg", false)) {
            showFragmentDialog(getString(R.string.NOTICE),getString(R.string.ebook_msg));
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean("showMsg", true);
            editor.apply(); // commit changes
        }
    }

    private void showFragmentDialog(String answer,String comment) {
        FragmentManager fm = getSupportFragmentManager();
        DialogFrag dialogFrag = DialogFrag.newInstance(answer,comment);
        dialogFrag.show(fm, "dialog_frag");
    }

    //Initialize ads , banner and interstitial
    private void initializeAdmobAds() {
        /******************************************************/
        // Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713

        MobileAds.initialize(this, getString(R.string.admob_appId));

        mAdView = findViewById(R.id.adView);

        mAdView.loadAd(new AdRequest.Builder().build());
/*
Log.v(); // Verbose
Log.d(); // Debug
Log.i(); // Info
Log.w(); // Warning
Log.e(); // Error
 */
        mAdView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                ignoreLayoutChange=true; //used to count properly layout changes
            }
        });

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                Log.i(LOG_TAG, "zzzz Banner loaded");
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
                Log.e(LOG_TAG, "zzzz Banner failed to load with code=" + errorCode);
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
                Log.i(LOG_TAG, "zzzz Ad Opened");
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
                Log.i(LOG_TAG, "zzzz Banner: User left the app");
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when when the user is about to return
                // to the app after tapping on an ad.
                Log.i(LOG_TAG, "zzzz ad Banner Closed");
            }
        });

        mAdmobInterstitial = new InterstitialAd(this);
        mAdmobInterstitial.setAdUnitId(getString(R.string.admob_interstitial));
        mAdmobInterstitial.loadAd(new AdRequest.Builder().build());
        mAdmobInterstitial.setAdListener(new AdListener(){

                                             @Override
                                             public void onAdClosed() {
                                                 // super.onAdClosed();
                                                 mAdmobInterstitial.loadAd(new AdRequest.Builder().build());
                                                 ignoreLayoutChange=true; //used to count properly layout changes
                                             }

                                             @Override
                                             public void onAdFailedToLoad(int i) {
                                                 //super.onAdFailedToLoad(i);
                                                 Log.e(LOG_TAG, "zzzz Interstitial Failed with code:" + i);
                                             }

                                             @Override
                                             public void onAdLeftApplication() {
                                                 //super.onAdLeftApplication();
                                                 Log.i(LOG_TAG, "zzz Ad left app");
                                             }

                                             @Override
                                             public void onAdOpened() {
                                                 //Renew ad when closed
                                                 //super.onAdOpened();
                                                 Log.i(LOG_TAG, "zzz Ad Opened");
                                                 ignoreLayoutChange=true; //used to count properly layout changes
                                             }

                                             @Override
                                             public void onAdLoaded() {
                                                 //super.onAdLoaded();
                                                 Log.i(LOG_TAG, "zzz Ad Loaded");
                                             }

                                             @Override
                                             public void onAdClicked() {
                                                 //super.onAdClicked();
                                                 Log.i(LOG_TAG, "zzz Ad Clicked");
                                             }

                                             @Override
                                             public void onAdImpression() {
                                                 //super.onAdImpression();
                                                 Log.i(LOG_TAG, "zzz Ad Impression");
                                             }
                                         }
        );
        Log.i("Add Init" ,"App.   Id:" + getString(R.string.admob_appId));
        Log.i("Add Init" ,"Banner Id:" + getString(R.string.admob_banner));
        Log.i("Add Init" ,"Inter. Id:" + getString(R.string.admob_interstitial));



    }

    private void initializeAppLovinAds() {

        final FolioActivity folioActivity = this;

        mApplovinInterstitial = AppLovinInterstitialAd.create( AppLovinSdk.getInstance( this ), this );
        mApplovinInterstitial.setAdDisplayListener(new AppLovinAdDisplayListener() {
            @Override
            public void adDisplayed(AppLovinAd ad) {

            }

            @Override
            public void adHidden(AppLovinAd ad) {
                //recreate the interstitial when it is closed.
                mApplovinInterstitial = AppLovinInterstitialAd.create( AppLovinSdk.getInstance( folioActivity ), folioActivity );
            }
        });
        mApplovinInterstitial.setAdClickListener(new AppLovinAdClickListener() {
            @Override
            public void adClicked(AppLovinAd ad) {

            }
        });

        mApplovinInterstitial.setAdVideoPlaybackListener(new AppLovinAdVideoPlaybackListener() {
            @Override
            public void videoPlaybackBegan(AppLovinAd ad) {

            }

            @Override
            public void videoPlaybackEnded(AppLovinAd ad, double percentViewed, boolean fullyWatched) {

            }
        });

    }

    private void initToolbar(Bundle savedInstanceState) {

        toolbar = findViewById(R.id.toolbar);
        toolbar.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                ignoreLayoutChange=true;
            }
        });
        toolbar.setListeners(this);
        if (savedInstanceState != null) {
            toolbar.setVisible(savedInstanceState.getBoolean(BUNDLE_TOOLBAR_IS_VISIBLE));
            if (toolbar.getVisible()) {
                toolbar.show();
            } else {
                toolbar.hide();
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Config config = AppUtil.getSavedConfig(getApplicationContext());
            int color;
            if (config.isNightMode()) {
                color = ContextCompat.getColor(this, R.color.black);
            } else {
                int[] attrs = {android.R.attr.navigationBarColor};
                TypedArray typedArray = getTheme().obtainStyledAttributes(attrs);
                color = typedArray.getColor(0, ContextCompat.getColor(this, R.color.white));
            }
            getWindow().setNavigationBarColor(color);
        }
    }

    @Override
    public void showMediaController() {
        mediaControllerView.show();
    }

    @Override
    public void startContentHighlightActivity() {
        Intent intent = new Intent(FolioActivity.this, ContentHighlightActivity.class);
        intent.putExtra(CHAPTER_SELECTED, mSpineReferenceList.get(mChapterPosition).href);
        intent.putExtra(FolioReader.INTENT_BOOK_ID, mBookId);
        intent.putExtra(Constants.BOOK_TITLE, bookFileName);
        startActivityForResult(intent, ACTION_CONTENT_HIGHLIGHT);
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }

    private void initBook(String mEpubFileName, int mEpubRawId, String mEpubFilePath, EpubSourceType mEpubSourceType) {
        try {
            int portNumber = getIntent().getIntExtra(Config.INTENT_PORT, Constants.PORT_NUMBER);
            mEpubServer = EpubServerSingleton.getEpubServerInstance(portNumber);
            mEpubServer.start();
            String path = FileUtil.saveEpubFileAndLoadLazyBook(FolioActivity.this, mEpubSourceType, mEpubFilePath,
                    mEpubRawId, mEpubFileName);
            addEpub(path);

            String urlString = Constants.LOCALHOST + bookFileName + "/manifest";
            new MainPresenter(this).parseManifest(urlString);

        } catch (IOException e) {
            Log.e(LOG_TAG, "initBook failed", e);
        }
    }

    private void addEpub(String path) throws IOException {
        Container epubContainer = new EpubContainer(path);
        mEpubServer.addEpub(epubContainer, "/" + bookFileName);
        getEpubResource();
    }

    private void getEpubResource() {
    }

    @Override
    public void onDirectionChange(@NonNull Config.Direction newDirection) {
        Log.v(LOG_TAG, "-> onDirectionChange");

        FolioPageFragment folioPageFragment = (FolioPageFragment)
                mFolioPageFragmentAdapter.getItem(mFolioPageViewPager.getCurrentItem());
        entryReadPosition = folioPageFragment.getLastReadPosition();

        direction = newDirection;


        mFolioPageViewPager.setDirection(newDirection);
        mFolioPageFragmentAdapter = new FolioPageFragmentAdapter(getSupportFragmentManager(),
                mSpineReferenceList, bookFileName, mBookId);
        mFolioPageViewPager.setAdapter(mFolioPageFragmentAdapter);
        mFolioPageViewPager.setCurrentItem(mChapterPosition);
    }

    @Override
    public void showConfigBottomSheetDialogFragment() {
        new ConfigBottomSheetDialogFragment().show(getSupportFragmentManager(),
                ConfigBottomSheetDialogFragment.class.getSimpleName());
    }

    @Override
    public void hideOrShowToolBar() {
        toolbar.showOrHideIfVisible();
    }

    @Override
    public void hideToolBarIfVisible() {}

    @Override
    public void setPagerToPosition(String href) {  }

    @Override
    public ReadPosition getEntryReadPosition() {
        if (entryReadPosition != null) {
            ReadPosition tempReadPosition = entryReadPosition;
            entryReadPosition = null;
            return tempReadPosition;
        }
        return null;
    }

    @Override
    public void goToChapter(String href) {
        href = href.substring(href.indexOf(bookFileName + "/") + bookFileName.length() + 1);
        for (Link spine : mSpineReferenceList) {
            if (spine.href.contains(href)) {
                mChapterPosition = mSpineReferenceList.indexOf(spine);
                mFolioPageViewPager.setCurrentItem(mChapterPosition);
                toolbar.setTitle(spine.getChapterTitle());
                break;
            }
        }
    }

    //20191001: Found that checkInterstitial is in the BAD place, AFTER 1 year of launching.
    //That leaded in problems when displaying interstitial and false placements for interstitials(!!!!)

    private void checkInterstitial() {
        //FZSM: first interstitial attempt
        //Criteria: probability 1 on 3 of displaying an interstitial
        //Date date = new Date();
        //Random rand = new Random();
        //Trick to change seed every second
        //rand.setSeed(date.getSeconds());
        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        // 20181018: changed back: 1 interstitial by three read pages.
        //That's because Google ads restrictions and to avoid excessive ads or also too low ads.
        //I need a wayto foresee and predict incoming based on frequency.
        adCounter = ++adCounter % INTERSTITIAL_EVERY;
          if (adCounter+1==INTERSTITIAL_EVERY-1) {
            //Display interstitial warning
            if (adIsReady()) {
                showWarning();
            }

        } else {
            //Hide interstitial warning
            adWarning.setVisibility(View.GONE);
        }

        if (adCounter!=(INTERSTITIAL_EVERY-1)) return;

        //int randomNum = (int)(Math.random() * 4);
        //Log.v(LOG_TAG, "-> onPageScrollStateChanged -> checkInterstitial -> random number = " + randomNum);
        // if (randomNum !=3) return; 20181012: Currently disabled since I detected too low income
        Log.v("TAG", "goToChapter XX detected");
        if (adIsReady()) {
            showAd();
            adCounter=0;
            Log.v("TAG", "Interstitial YY show");
        } else {
            Log.v("TAG", "The interstitial ZZ failed");
        }
    }

    private void showAd() {
        Log.i("showAd","adnetwork:" + adnetwork );
        switch(adnetwork) {
            case "":
                Log.i("showAd","admob showad" );
                mAdmobInterstitial.show();
                break;
            case "admob":
                Log.i("showAd","admob showad" );
                mAdmobInterstitial.show();
                break;
            case "applovin":
                Log.i("showAd","applovin showad" );
                mApplovinInterstitial.show();
                break;
            case "none":
                Log.i("showAd","admob none" );
                break;

            default:
                Log.i("showAd","????" );
                break;
        }
    }

    private boolean adIsReady() {
        switch(adnetwork) {
            case "":
                Log.i("adIsReady","adisReady checked:" + mAdmobInterstitial.isLoaded());
                return mAdmobInterstitial.isLoaded();
            case "admob":
                Log.i("adIsReady","adisReady checked:" + mAdmobInterstitial.isLoaded());
                return mAdmobInterstitial.isLoaded();
            case "applovin":
                return (mApplovinInterstitial.isAdReadyToDisplay());
            case "none":
                return false;
            default:
                return false;

        }
    }

    //animate a simple msg warning.
    private void showWarning(){
        adWarning.setVisibility(View.VISIBLE);
        adWarning.setY(-40.0f); //exactly same as height
        adWarning.setAlpha(1.0f);
        //adWarning.animate().translationY(0.0f).alpha(1.0f).setDuration(1000);

        ObjectAnimator a1 = ObjectAnimator.ofFloat(adWarning,"translationY",0.0f);
        a1.setDuration(500);

        ObjectAnimator a2 = ObjectAnimator.ofFloat(adWarning,"alpha",1.0f);
        a2.setDuration(5000);

        /*20190923: the object would never disappear to avoid any complains.
        ObjectAnimator a3 = ObjectAnimator.ofFloat(adWarning,"translationY",-40.0f);
        a3.setDuration(500);
*/
        AnimatorSet as = new AnimatorSet();
        as.playSequentially(a1,a2);
        as.start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ACTION_CONTENT_HIGHLIGHT && resultCode == RESULT_OK && data.hasExtra(TYPE)) {

            String type = data.getStringExtra(TYPE);

            if (type.equals(CHAPTER_SELECTED)) {

                // checkInterstitial(); Display interstitial when use select a page on the menu.

                String selectedChapterHref = data.getStringExtra(SELECTED_CHAPTER_POSITION);
                for (Link spine : mSpineReferenceList) {
                    if (selectedChapterHref.contains(spine.href)) {
                        mChapterPosition = mSpineReferenceList.indexOf(spine);
                        mFolioPageViewPager.setCurrentItem(mChapterPosition);
                        FolioPageFragment folioPageFragment = (FolioPageFragment)
                                mFolioPageFragmentAdapter.getItem(mChapterPosition);
                        folioPageFragment.scrollToFirst();
                        folioPageFragment.scrollToAnchorId(selectedChapterHref);
                        break;
                    }
                }
            } else if (type.equals(HIGHLIGHT_SELECTED)) {
                HighlightImpl highlightImpl = data.getParcelableExtra(HIGHLIGHT_ITEM);
                mFolioPageViewPager.setCurrentItem(highlightImpl.getPageNumber());
                FolioPageFragment folioPageFragment = (FolioPageFragment)
                        mFolioPageFragmentAdapter.getItem(highlightImpl.getPageNumber());
                folioPageFragment.scrollToHighlightId(highlightImpl.getRangy());
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (outState != null)
            outState.putParcelable(BUNDLE_READ_POSITION_CONFIG_CHANGE, lastReadPosition);

        if (mEpubServer != null) {
            mEpubServer.stop();
        }
    }

    @Override
    public int getChapterPosition() {
        return mChapterPosition;
    }

    @Override
    public void onLoadPublication(EpubPublication publication) {
        mSpineReferenceList.addAll(publication.spines);
        if (publication.metadata.title != null) {
            toolbar.setTitle(publication.metadata.title);
        }

        if (mBookId == null) {
            if (publication.metadata.identifier != null) {
                mBookId = publication.metadata.identifier;
            } else {
                if (publication.metadata.title != null) {
                    mBookId = String.valueOf(publication.metadata.title.hashCode());
                } else {
                    mBookId = String.valueOf(bookFileName.hashCode());
                }
            }
        }
        configFolio();
    }

    private void configFolio() {

        mFolioPageViewPager = findViewById(R.id.folioPageViewPager);
        //wvp =(WebViewPager) mFolioPageViewPager.getAdapter().getClass();

        // Replacing with addOnPageChangeListener(), onPageSelected() is not invoked


/***20191001: After SEVERAL days attempting to find a SIMPLE way of counting interactions, it seems LayoutChange would do the trick*/

        mFolioPageViewPager.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (ignoreLayoutChange) {
                   // Log.v(LOG_TAG, "-> onLayoutChange  ignored");
                    ignoreLayoutChange=false;
                    return;
                }
                Log.v(LOG_TAG, "-> onLayoutChange  " + adCounter);
               // checkInterstitial();
            }
        });


        /****/



        mFolioPageViewPager.setOnPageChangeListener(new DirectionalViewpager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Log.v(LOG_TAG, "-> onPageScrolled  " + adCounter);
                /*
               Log.v(LOG_TAG, " onPageScrolled pos:" + String.valueOf(position) + " posOffset" + String.valueOf(positionOffset) + " posOffsetPixels:" +    String.valueOf(positionOffsetPixels));

                if ((position !=lastPosition) && (position>lastPosition)) {
                    Log.v(LOG_TAG, "-> onPageScrolled -> position = " + position + " lastPos" + lastPosition);
                    lastPosition = position;
                    //checkInterstitial(); //If scroll forward, show interstitial. Better & more clever.
                }
                */
            }

            @Override
            public void onPageSelected(int position) {
                Log.v(LOG_TAG, "-> onPageSelected -> DirectionalViewpager -> position = " + position);
                ignoreLayoutChange=true;
                EventBus.getDefault().post(new MediaOverlayPlayPauseEvent(
                        mSpineReferenceList.get(mChapterPosition).href, false, true));
                mediaControllerView.setPlayButtonDrawable();
                mChapterPosition = position;
                toolbar.setTitle(mSpineReferenceList.get(mChapterPosition).bookTitle);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                Log.v(LOG_TAG, "-> onPageScrollStateChanged -> DirectionalViewpager -> " +  "position = " + state);

                if (state == DirectionalViewpager.SCROLL_STATE_IDLE) {
                    int position = mFolioPageViewPager.getCurrentItem();



                    //checkInterstitial(); //Display interstitial every time, not very good...

                    FolioPageFragment folioPageFragment =
                            (FolioPageFragment) mFolioPageFragmentAdapter.getItem(position - 1);
                    if (folioPageFragment != null) {
                        //Restriction:only show ads if we advance a page, not going backward
                        Log.v("onPageScrollStateChange", "position:" + String.valueOf(position) + "lastP" + String.valueOf(lastPosition));


                        folioPageFragment.scrollToLast();
                    }

                    folioPageFragment =
                            (FolioPageFragment) mFolioPageFragmentAdapter.getItem(position + 1);
                    if (folioPageFragment != null)
                        folioPageFragment.scrollToFirst();
                }
            }

            @Override
            public void onPageViewScrolled(int position) {
                Log.v(LOG_TAG, "-> onPageViewScrolled ->" + position);
            }
        });

        if (mSpineReferenceList != null) {

            mFolioPageViewPager.setDirection(direction);
            mFolioPageFragmentAdapter = new FolioPageFragmentAdapter(getSupportFragmentManager(),
                    mSpineReferenceList, bookFileName, mBookId);

            mFolioPageViewPager.setAdapter(mFolioPageFragmentAdapter);

//            WebViewPager wvp = mFolioPageFragmentAdapter
            //WebViewPager wvp =mFolioPageViewPager.g



            ReadPosition readPosition;
            if (savedInstanceState == null) {
                readPosition = getIntent().getParcelableExtra(FolioActivity.EXTRA_READ_POSITION);
                entryReadPosition = readPosition;
            } else {
                readPosition = savedInstanceState.getParcelable(BUNDLE_READ_POSITION_CONFIG_CHANGE);
                lastReadPosition = readPosition;
            }
            mFolioPageViewPager.setCurrentItem(getChapterIndex(readPosition));
        }
    }

    /**
     * Returns the index of the chapter by following priority -
     * 1. id
     * 2. href
     * 3. index
     *
     * @param readPosition Last read position
     * @return index of the chapter
     */
    private int getChapterIndex(ReadPosition readPosition) {
        if (readPosition == null) {
            return 0;

        } else if (!TextUtils.isEmpty(readPosition.getChapterId())) {
            return getChapterIndex("id", readPosition.getChapterId());

        } else if (!TextUtils.isEmpty(readPosition.getChapterHref())) {
            return getChapterIndex("href", readPosition.getChapterHref());

        } else if (readPosition.getChapterIndex() > -1
                && readPosition.getChapterIndex() < mSpineReferenceList.size()) {
            return readPosition.getChapterIndex();
        }

        return 0;
    }

    private int getChapterIndex(String caseString, String value) {
        for (int i = 0; i < mSpineReferenceList.size(); i++) {
            switch (caseString) {
                case "id":
                    if (mSpineReferenceList.get(i).getId().equals(value))
                        return i;
                case "href":
                    if (mSpineReferenceList.get(i).getOriginalHref().equals(value))
                        return i;
            }
        }
        return 0;
    }

    /**
     * If called, this method will occur after onStop() for applications targeting platforms
     * starting with Build.VERSION_CODES.P. For applications targeting earlier platform versions
     * this method will occur before onStop() and there are no guarantees about whether it will
     * occur before or after onPause()
     *
     * @see Activity#onSaveInstanceState(Bundle) of Build.VERSION_CODES.P
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.v(LOG_TAG, "-> onSaveInstanceState");
        this.outState = outState;

        outState.putBoolean(BUNDLE_TOOLBAR_IS_VISIBLE, toolbar.getVisible());
    }

    @Override
    public void storeLastReadPosition(ReadPosition lastReadPosition) {
        Log.v(LOG_TAG, "-> storeLastReadPosition");
        this.lastReadPosition = lastReadPosition;
    }

    private void setConfig(Bundle savedInstanceState) {

        Config config;
        Config intentConfig = getIntent().getParcelableExtra(Config.INTENT_CONFIG);
        boolean overrideConfig = getIntent().getBooleanExtra(Config.EXTRA_OVERRIDE_CONFIG, false);
        Config savedConfig = AppUtil.getSavedConfig(this);

        if (savedInstanceState != null) {
            config = savedConfig;

        } else if (savedConfig == null) {
            if (intentConfig == null) {
                config = new Config();
            } else {
                config = intentConfig;
            }

        } else {
            if (intentConfig != null && overrideConfig) {
                config = intentConfig;
            } else {
                config = savedConfig;
            }
        }

        // Code would never enter this if, just added for any unexpected error
        // and to avoid lint warning
        if (config == null)
            config = new Config();

        AppUtil.saveConfig(this, config);
        direction = config.getDirection();
    }

    @Override
    public void play() {
        EventBus.getDefault().post(new MediaOverlayPlayPauseEvent(mSpineReferenceList.get(mChapterPosition).href, true, false));
    }

    @Override
    public void pause() {
        EventBus.getDefault().post(new MediaOverlayPlayPauseEvent(mSpineReferenceList.get(mChapterPosition).href, false, false));
    }

    @Override
    public void onError() {
    }

    private void setupBook() {
        bookFileName = FileUtil.getEpubFilename(this, mEpubSourceType, mEpubFilePath, mEpubRawId);
        initBook(bookFileName, mEpubRawId, mEpubFilePath, mEpubSourceType);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constants.WRITE_EXTERNAL_STORAGE_REQUEST:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setupBook();
                } else {
                    Toast.makeText(this, getString(R.string.cannot_access_epub_message), Toast.LENGTH_LONG).show();
                    finish();
                }
                break;
        }
    }



    @Override
    public Config.Direction getDirection() {
        return direction;
    }
}

