package com.folioreader.ui.folio.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.folioreader.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnDialogDismissListener} interface
 * to handle interaction events.
 * Use the {@link DialogFrag#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DialogFrag extends DialogFragment {

    private TextView dialogLabel;
    private TextView dialogText;
    private Button btnOk;

    private OnDialogDismissListener mCallback;

    public interface OnDialogDismissListener {
        // TODO: Update argument type and name
        public void onDialogDismissListener(int position);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnDialogDismissListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnDialogDismissListener");
        }
    }

    public DialogFrag() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    public static DialogFrag newInstance(String label, String text) {
        DialogFrag frag = new DialogFrag();
        Bundle args = new Bundle();
        args.putString("label", label);
        args.putString("text", text);
        frag.setArguments(args);
        return frag;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_modal_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnOk = view.findViewById(R.id.btnOk);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onDialogDismissListener(0);
              dismiss();
            }
        });
        // Get field from view
        dialogLabel =view.findViewById(R.id.dialog_label);
        dialogText = view.findViewById(R.id.dialog_text);

        // Fetch arguments from bundle and set title
        String label = getArguments().getString("label", "(No answer found)");
        //  getDialog().setTitle("DONDE SE VE ESTO?");
        dialogLabel.setText(label);

        String text = getArguments().getString("text","(No text found)");
        dialogText.setText(text);
    }


}
