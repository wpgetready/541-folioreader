package com.alexandriabookseng.ebook;
/*
20190403: So this class is needed for properly configuring OneSignal.
However , this app is already using multidexapplication
So initially the app was saying in AndroidManifest.xml:
   < application android:name="android.support.multidex.MultiDexApplication"
    ...
    ...

    And if we go to class definition, which is read-only
    package android.support.multidex;

    import android.app.Application;
    import android.content.Context;

    public class MultiDexApplication extends Application {
        public MultiDexApplication() {
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}

So the the solution of the riddle is creating an App class mimicking MultiDexApplication
and start from there, adding what OneSignal is needing

 */

import android.app.Application;
import android.content.Context;
//import android.support.multidex.MultiDex; //FZSM 20190430: Needs multidex version 1.0.2 in order to work


import com.onesignal.OneSignal;

public class App extends Application{
    public App() {

    }

    @Override
    public void onCreate() {
        super.onCreate();
        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
       // MultiDex.install(this);
    }

}
