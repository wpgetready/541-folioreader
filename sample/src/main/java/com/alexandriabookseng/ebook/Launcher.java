package com.alexandriabookseng.ebook;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;

import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.folioreader.Config;
import com.folioreader.FolioReader;
import com.folioreader.android.sample.R;
import com.folioreader.model.HighLight;
import com.folioreader.model.ReadPosition;
import com.folioreader.model.ReadPositionImpl;
import com.folioreader.ui.base.OnSaveHighlight;
import com.folioreader.util.AppUtil;
import com.folioreader.util.ObjectMapperSingleton;
import com.folioreader.util.OnHighlightListener;
import com.folioreader.util.ReadPositionListener;
import com.onesignal.OneSignal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Fer on 8/3/2018.
 */

public class Launcher extends AppCompatActivity
        implements OnHighlightListener, ReadPositionListener {
    private FolioReader folioReader;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//20190430: Added OneSignal. all the libraries belong to FolioReader, the use is inherited from there(!).

        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();

        setContentView(R.layout.test);
        TextView tv= (TextView) findViewById(R.id.textView1);
        //tv.setText(Html.fromHtml(getString(R.string.endtext)));
        String hlink =getString(R.string.linkGooglePlay).trim();
        //Special case: the very FIRST time we do the ebook there is no link, but it will be next time.
        //So if the link is empty, then don't make a link
        if (hlink.equals("")) {
            tv.setText(Html.fromHtml("<h1>Alexandria Books (ENG)</h1><br/>Thank you for reading.<br/>If you enjoy it, please rate us positively.<br/>Have a great day! <br/><br/>(press back again to exit)"));
        }
        else
        {
            tv.setText(Html.fromHtml("<h1>Alexandria Books (ENG)</h1><br/>Thank you for reading.<br/>If you enjoy it, <a href='" + getString(R.string.linkGooglePlay)
                    + "'>please rate us positively</a>.<br/>Have a great day! <br/><br/>(press back again to exit)"));
        }

        tv.setMovementMethod(LinkMovementMethod.getInstance());

        folioReader = FolioReader.getInstance(getApplicationContext())
                .setOnHighlightListener(this)
                .setReadPositionListener(this);

        getHighlightsAndSave();
        AutoStart();
    }

    @Override
    public void saveReadPosition(ReadPosition readPosition) {

        //Toast.makeText(this, "ReadPosition = " + readPosition.toJson(), Toast.LENGTH_SHORT).show();
        Log.i("Launcher", "-> ReadPosition = " + readPosition.toJson());
    }

    @Override
    public void onHighlight(HighLight highlight, HighLight.HighLightAction type) {
        //Toast.makeText(this, "highlight id = " + highlight.getUUID() + " type = " + type, Toast.LENGTH_SHORT).show();
        Log.i("Launcher", "highlight id = " + highlight.getUUID() + " type = " + type);
    }

    /*
     * For testing purpose, we are getting dummy highlights from asset. But you can get highlights from your server
     * On success, you can save highlights to FolioReader DB.
     */
    private void getHighlightsAndSave() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                ArrayList<HighLight> highlightList = null;
                ObjectMapper objectMapper = new ObjectMapper();
                try {
                    highlightList = objectMapper.readValue(
                            loadAssetTextAsString("highlights/highlights_data.json"),
                            new TypeReference<List<HighlightData>>() {
                            });
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (highlightList == null) {
                    folioReader.saveReceivedHighLights(highlightList, new OnSaveHighlight() {
                        @Override
                        public void onFinished() {
                            //You can do anything on successful saving highlight list
                        }
                    });
                }
            }
        }).start();
    }

    private void AutoStart() {
        ReadPosition readPosition = getLastReadPosition();
        String getEpubFile ="";
        Config config = AppUtil.getSavedConfig(getApplicationContext());
        if (config == null)
            config = new Config();

        config.setAllowedDirection(Config.AllowedDirection.VERTICAL_AND_HORIZONTAL);
        try {
            getEpubFile = epubPath();
            if(getEpubFile.length()!=0) {
                folioReader.setReadPosition(readPosition)
                        .setConfig(config, true)
                        .openBook("file:///android_asset/" + getEpubFile.trim());
            } else
            {
                Log.w("FOLIOREADER", "epub file not found!!!");
            }
        } catch (IOException e) {
            Log.e("FOLIOREADER", e.getMessage());
            //e.printStackTrace();
        }
    }


    //Scans for an epub file in the assets folder. Return first epub found or empty string or error.
    private String epubPath() throws IOException {
        String[] am =getAssets().list("");
        String epubFN = "";
        //Iterate assets in the assets folders (only in the root)
        for (String assetFile: am) {
            if (assetFile.endsWith(".epub")){
                epubFN =assetFile;
                break;
            }
        }
        return epubFN;
    }

    private ReadPosition getLastReadPosition() {

        ReadPosition readPosition = null;
        ObjectReader objectReader = ObjectMapperSingleton.getObjectMapper().reader();

        try {
            readPosition = objectReader.forType(ReadPositionImpl.class)
                    .readValue(getAssets().open("read_positions/read_position.json"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return readPosition;
    }

    private String loadAssetTextAsString(String name) {
        BufferedReader in = null;
        try {
            StringBuilder buf = new StringBuilder();
            InputStream is = getAssets().open(name);
            in = new BufferedReader(new InputStreamReader(is));

            String str;
            boolean isFirst = true;
            while ((str = in.readLine()) != null) {
                if (isFirst)
                    isFirst = false;
                else
                    buf.append('\n');
                buf.append(str);
            }
            return buf.toString();
        } catch (IOException e) {
            Log.e("Launcher", "Error opening asset " + name);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    Log.e("Launcher", "Error closing asset " + name);
                }
            }
        }
        return null;
    }


}
